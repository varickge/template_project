This is test repository, follow instructions please:

To edit the file online, click "Open in Web IDE".
Alternatively, you can clone the repository to your local machine for offline work.
Make the necessary changes, commit them, and push to the repository.
Head back to the original course page and access the "Structure" tab.
Click on "Example_task" and press the "Submit solution" button.
Check the results under the "test" stage in UI. If your solution passed the tests, proceed.
Finally, click the "Finish" button to conclude the task.
If your solution didn't pass the tests, revisit the previous steps to make necessary adjustments and resubmit.
